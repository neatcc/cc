# for arm build
ARCH = -DNEATCC_ARM
GEN = arch/arm.o

# for x86 build
#ARCH = -DNEATCC_X86
#GEN = arch/x86.o

# for x86_64 build
#ARCH = -DNEATCC_X64
#GEN = arch/x64.o

CC = clang
CFLAGS = -Wall -Werror -Wno-macro-redefined -Wfatal-errors -O2 $(ARCH)

#CC = gcc
#CFLAGS = -Wall -Werror -Wfatal-errors -O2 $(ARCH)

#CC = musl-gcc
#CFLAGS = -Wall -Werror -Wfatal-errors -O2 $(ARCH)

LDFLAGS =

all: ncc npp
%.o: %.c ncc.h
	$(CC) -c $(CFLAGS) $< -o $*.o

# $^ "internal macro" gives a space separated list of command prerequisites
# however this feature doesn't seem to be required in POSIX
# http://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html

ncc: ncc.o tok.o out.o cpp.o gen.o reg.o mem.o $(GEN)
	$(CC) ncc.o tok.o out.o cpp.o gen.o reg.o mem.o $(GEN) $(LDFLAGS) -o $@
npp: npp.o cpp.o
	$(CC) npp.o cpp.o $(LDFLAGS) -o $@

clean:
	rm -f arch/*.o *.o

distclean:
	rm -f arch/*.o *.o ncc npp

